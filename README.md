# Tedious::Edits 

## Description

Tedious::Edits is a little DSL to help with the laborious task of updating source files with small but important edits.

## Usage

Given a source file such as:

```perl
use My::Database;

if (1) {
  open(MAIL, "|/usr/bin/sendmail -t");
  print MAIL "Subject: Hello\n";
  close(MAIL);
}
print "All done!\n";

exit 255;
```

And a `Tedious::Edits` file to apply edits to it (and presumably, many other similar files):

```perl
use Tedious::Edits v0.2.1;

my $edits = Edits(
  # Context here is entire document, so inserts at the top of the file.
  InsertBefore('# This is a comment inserted at the top of the file'),

  # Use Between(regexp, regexp, actions) to isolate a sub-section of the document to be edited.
  Between(qw/open\(MAIL/, qw/close\(MAIL/,
    # The Insert* directives can take an 'indent' option, 0 meaning 'the same indent as the nearby line'.
    Options(indent => 0),

    # The specified range of lines is inclusive, so InsertBefore goes before the 'open(MAIL' line.
    InsertBefore('if (0) {',
                 '  say STDERR "lol";',
                 '}',
                 'my $mailout;',
                ),

    # Likewise, this line will be added after the 'close(MAIL' line.
    InsertAfter('SendViaSendmail($mailout);'),
  ),
);

$edits->apply_to_file('example.pl');
```

You should end up with a file that now looks like:

```perl
# This is a comment inserted at the top of the file
use My::Database;

if (1) {
  if (0) {
    say STDERR "lol";
  }
  my $mailout;
  open(MAIL, "|/usr/bin/sendmail -t");
  print MAIL "Subject: Hello\n";
  close(MAIL);
  SendViaSendmail($mailout);
}
print "All done!\n";

exit 255;
```


## Dependencies

You need `Class::Tiny`, `Method::Signatures`, `File::Slurp`, and `Term::ANSIColor`. If you're using `cpanm`, you can run `cpanm --installdeps .` in the repo root.

## Copyright

Copyright (c) 2019 James Clark

This software may be distributed under the same terms as Perl itself.

