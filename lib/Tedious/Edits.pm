# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits;
use version; our $VERSION = 'v0.2.1';

use v5.10;
use strict;
use warnings;
use Carp;
use Exporter qw/import/;

use Method::Signatures;

use Tedious::Edits::EditPlan;
use Tedious::Edits::Selection;
use Tedious::Edits::Options;
use Tedious::Edits::Insertion;
use Tedious::Edits::Modification;

our @EXPORT = qw/Edits Between Options InsertBefore InsertAfter Modify/;


func Edits(@edits) {
  my ($package, $filename, $line) = caller;
  my $plan = Tedious::Edits::EditPlan->new(source => "$filename:$line", edits => \@edits);
  #### ????
  return $plan;
}


func Between($re1, $re2, @actions) {
  my ($package, $filename, $line) = caller;
  return Tedious::Edits::Selection->new(source => "$filename:$line", re1 => $re1, re2 => $re2, actions => \@actions);
}


# Set options on this context and all nested ones with sub-selections.
func Options(Int :$indent) {
  my ($package, $filename, $line) = caller;
  return Tedious::Edits::Options->new(source => "$filename:$line", options => { indent => $indent });
}


# options:
#  indent => 0 (if set at all, takes indent from the line above, modified +/- by the key's value)
func InsertBefore(Str @text) {
  my ($package, $filename, $line) = caller;
  return Tedious::Edits::Insertion->new(source => "$filename:$line", text => \@text, where => 'BEFORE');
}

# options:
#  indent => 0 (if set at all, takes indent from the line above, modified +/- by the key's value)
func InsertAfter(Str @text) {
  my ($package, $filename, $line) = caller;
  return Tedious::Edits::Insertion->new(source => "$filename:$line", text => \@text, where => 'AFTER');
}


func Modify(CodeRef $coderef) {
  my ($package, $filename, $line) = caller;
  return Tedious::Edits::Modification->new(source => "$filename:$line", coderef => $coderef);
}


1;
