# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::EditPlan;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;
use File::Slurp;
use File::Copy 'cp';

use Tedious::Edits::Context;

use Class::Tiny qw/
    source
    edits
  /;


method apply_to_file($filename) {
  # Create a backup file first.
  die "$filename.bak already exists, refusing to overwrite." if -f "$filename.bak";
  cp $filename, "$filename.bak" or die "Making backup file '$filename.bak' failed: $!";
  
  # Read the file with File::Slurp as a list; this leaves us with newlines which we will want to remove.
  my @lines = read_file($filename);
  chomp @lines;
  
  @lines = $self->apply_to_lines(@lines);
  
  # Reapply newlines that write_file expects and save the new file.
  @lines = map { "$_\n" } @lines;
  write_file($filename, { atomic => 1 }, @lines);
}


method apply_to_string(Str $string) {
  # Split the string up into lines without '\n's - which might result in a single "" line at the end if there's a trailing newline.
  my @lines = split("\n", $string, -1);  # perldoc -f split; we need to allow trailing empty fields.
  @lines = $self->apply_to_lines(@lines);
  # Then glue them back together.
  return join("\n", @lines);
}


method apply_to_lines(@lines) {
  # Build the outermost Context object and apply the edits.
  my $context = Tedious::Edits::Context->new(lines => \@lines, fragment_start => 0, fragment_end => @lines - 1);
  $self->apply($context);
  return @{$context->lines};
}


method apply($context) {
  $context->note("Applying Edits");
  foreach my $edit (@{$self->edits}) {
    $edit->apply($context);
  }
}


1;

