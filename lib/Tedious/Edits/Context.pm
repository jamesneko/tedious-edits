# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::Context;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;
use Term::ANSIColor;

use Class::Tiny qw/
    fragment_start
    fragment_end
    parent
  /,
  {
    lines     => sub { [] },
    options   => sub { {} },
    stacksize => sub { 0 },
  };


method clone(%args) {
  $args{fragment_start} //= $self->fragment_start;
  $args{fragment_end}   //= $self->fragment_end;
  $args{lines}          //= [ @{$self->lines} ];
  $args{options}        //= { %{$self->options} };
  $args{stacksize}      //= 1;
  return __PACKAGE__->new(%args, parent => $self, stacksize => $args{stacksize} + 1);
}


method option(Str $name) {
  return $self->options->{$name};
}


method note($string, $colour = 'cyan') {
  say STDERR ('  ' x $self->stacksize) . colored($string, $colour);
}


1;

