# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::Modification;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;

use Class::Tiny qw/
    source
    coderef
  /;


method apply($context) {
  my $coderef = $self->coderef;
  for (my $i = 0; $i < @{$context->lines}; $i++) {
    my $original = $context->lines->[$i];
    my $modified = $coderef->($original);
    if ($modified ne $original) {
      $context->note("Coderef modified line " . ($context->fragment_start + $i) . ": '$original' -> '$modified'");
      $context->lines->[$i] = $modified;
    }
  }
}


1;
