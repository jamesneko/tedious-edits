# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::Selection;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;

use Tedious::Edits::Context;

use Class::Tiny qw/
    source
    re1
    re2
    actions
  /;

method apply($context) {
  my $re1 = $self->re1;
  my $re2 = $self->re2;
  $context->note("Applying Selection: Between $re1 and $re2");
  my @lines = @{$context->lines};
  my ($start, $end);
  
  for (my $i = 0; $i < @lines; $i++) {
    my $actual_linenum = $context->fragment_start + $i;
    if ( ! defined $start) {
      if ($lines[$i] =~ $re1) {
        $context->note("  $actual_linenum: /$re1/ matches '$lines[$i]'");
        $start = $i;
      }
    } elsif ( ! defined $end) {
      if ($lines[$i] =~ $re2) {
        $context->note("  $actual_linenum: /$re2/ matches '$lines[$i]'");
        $end = $i;
        
        # Found our selection! Extract it into its own context.
        my @subset = @lines[$start .. $end];  # Inclusive range, array slice
        my $subcontext = $context->clone(lines => \@subset, fragment_start => $start, fragment_end => $end);
        
        # Apply all our actions to this selection
        foreach my $edit (@{$self->actions}) {
          $edit->apply($subcontext);
        }
        
        # Splice those edits back into the main @lines.
        my $length = $end - $start + 1;  # Inclusive range
        splice @lines, $start, $length, @{$subcontext->lines};
        
        # Update which line we were up to to be at the end of the matching block (after edits have updated the selection)
        $i = $start + @{$subcontext->lines} -1;
        $context->note("Resuming search for matching blocks from after new line $i ('" . $lines[$i] . "')");
        
        # Look for further matching blocks.
        undef $start;
        undef $end;
      }
    }
  }
  $context->lines([ @lines ]);
}


1;
