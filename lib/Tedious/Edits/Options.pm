# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::Options;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;

use Class::Tiny qw/
    source
    options
  /;


method apply($context) {
  die $self->source . ": No options to set!" unless $self->options && ref $self->options eq 'HASH';
  $context->options({ %{$context->options}, %{$self->options} });
  $context->note("Setting Options: " . join(", ", map { "$_ => " . $self->options->{$_} } keys %{$self->options}));
}


1;
