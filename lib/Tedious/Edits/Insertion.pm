# Tedious::Edits Copyright (c) 2019 James Clark
# This software may be distributed under the same terms as Perl itself.
package Tedious::Edits::Insertion;

use v5.10;
use strict;
use warnings;
use Carp;

use Method::Signatures;

use Class::Tiny qw/
    source
    where
  /,
  {
    text => sub { [] },
  };


method apply($context) {
  if ($self->where eq 'BEFORE') {
    my @text = @{$self->text};
    $context->note("Inserting before line " . $context->fragment_start . ": '" . $text[0] . "'" . (@text > 1 ? ", ..." : ""));
    if (defined $context->option('indent')) {
      my $indent = extract_indent($context->lines->[0], $context->option('indent'));
      @text = map { apply_indent($_, $indent) } @text;
    }
    
    unshift @{$context->lines}, @text;
    
  } elsif ($self->where eq 'AFTER') {
    my @text = @{$self->text};
    $context->note("Inserting after line " . $context->fragment_end . ": '" . $text[0] . "'" . (@text > 1 ? ", ..." : ""));
    if (defined $context->option('indent')) {
      my $indent = extract_indent($context->lines->[-1], $context->option('indent'));
      @text = map { apply_indent($_, $indent) } @text;
    }

    push @{$context->lines}, @text;

  } else {
    confess "Don't know how to insert this: '" . $self->where . "'";
  }
}


func extract_indent(Str $str, Int $mod = 0) {
  my $indent = "";
  if ($str =~ /^(\s*)/m) {
    $indent = $1;
    $indent =~ s/\t/        /g;
  }
  if ($mod > 0) {
    $indent .= " " x $mod;
  } elsif ($mod < 0) {
    $indent = substr $indent, 0, 0 - $mod;
  }
  return $indent;
}


func apply_indent(Str $text, Str $indent) {
  # Determine outermost indent in the existing text
  my $existing;
  while ($text =~ /^(\s*)/mg) {
    $existing //= $1;
    $existing = $1 if length($1) < length($existing);
  }
  # Replace it with the desired indent
  $text =~ s/^$existing/$indent/mg;
  return $text;
}


1;
