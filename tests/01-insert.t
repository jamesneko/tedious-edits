use v5.10;
use warnings;
use strict;
use Test::More tests => 1;

use Tedious::Edits;

# ======== ORIGINAL ========
my $orig = <<'EOF';
#!/usr/bin/perl
use My::Database;

if (1) {
  open(MAIL, "|/usr/bin/sendmail -t");
  print MAIL "Subject: Hello\n";
  close(MAIL);
}
print "All done!\n";
EOF

# ======== EDITS ========
my $edited = Edits(
  Between(qw/open\(MAIL/, qw/close\(MAIL/,
    Options(indent => 0),
    InsertBefore('# rather than send directly to pipe, write to a string:',
                 'my $mailout;',
               ),
    InsertAfter('SendViaSendmail($mailout);'),
  ),
)->apply_to_string($orig);

# ======== EXPECTED ========
my $expected = <<'EOF';
#!/usr/bin/perl
use My::Database;

if (1) {
  # rather than send directly to pipe, write to a string:
  my $mailout;
  open(MAIL, "|/usr/bin/sendmail -t");
  print MAIL "Subject: Hello\n";
  close(MAIL);
  SendViaSendmail($mailout);
}
print "All done!\n";
EOF

is $edited, $expected, 'Basic insertion test';

    