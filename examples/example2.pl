#!/usr/bin/env perl
use warnings;
use strict;
use v5.10;
use lib '../lib';

use Tedious::Edits v0.2.0;

# A place to stash something we capture while modifying the open(MAIL line.
my $stash = {};

my $edits = Edits(
  InsertBefore('use My::Email::Utils qw/SendViaSendmail/;'),
  Between(qw/open\(MAIL/, qw/close\(MAIL/,
    Options(indent => 0),
    InsertBefore("# rather than send directly to pipe, write to a string:",
                 "if (0) {",
                 "  say STDERR 'lol'",
                 "}",
                 'my $mailout;',
                ),
    Modify(sub {
      my $line = shift;
      $line =~ s/open\(MAIL,\s*"\|(.*?)"\);/open(MAIL, '>', \\\$mailout);/;
      $stash->{sendmail_args} = $1;
      return $line;
    }),
    InsertAfter('SendViaSendmail($mailout);',
                'say "Done!";',
               ),
    Modify(sub {
      say "hi i'm 2nd modify";
      my $line = shift;
      $line =~ s/SendViaSendmail\(.mailout\);/SendViaSendmail(\$mailout, pipe => "$stash->{sendmail_args}");/;
      return $line;
    }),
  ),
);

$edits->apply_to_file('edited2.pl');
