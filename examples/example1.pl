#!/usr/bin/env perl
use warnings;
use strict;
use v5.10;
use lib '../lib';

use Tedious::Edits v0.2.0;

my $edits = Edits(
  InsertBefore('use My::Email::Utils qw/SendViaSendmail/;'),
  Between(qw/open\(MAIL/, qw/close\(MAIL/,
    Options(indent => 0),
    InsertBefore("# rather than send directly to pipe, write to a string:",
                 "if (0) {",
                 "  say STDERR 'lol'",
                 "}",
                 'my $mailout;',
               ),
    InsertAfter('SendViaSendmail($mailout);'),
  ),
);

$edits->apply_to_file('edited1.pl');
